1. First commit:
  - git commit -a -m 'First commit'
2. Second commit:
  - git commit -a -m 'Second commit'
3. Create new branch:
  - git checkout -b awesome-feature-branch 
  - edit files
  - commit changes in the branch
  - git commit -a -m 'Awesome feature changes commit'
3.1 Checkout master, make changes in the master, creating commit 3:
  - git checkout master
  - git commit -a -m 'Third commit'
4. Merge awesome-feature-branch into master, creating merge commit:
  - git merge awesome-feature-branch
5. Fourth commit:
  - edit file
  - git commit -a -m 'Fourth commit'
6. Resulting commit log can be viewed as a graph:
  - git log --graph
